﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CsvHelper;
using CsvHelper.Configuration;
using System.IO;
using System.Text;
using System.Linq;

/// <summary>
/// Class that wraps CsvHelper, and can read arbitrary csv files into Objects.
/// CsvHelper: https://joshclose.github.io/CsvHelper/
/// </summary>
public class CSVUtils
{
    private static Configuration config;
    
    static CSVUtils() {
        config = new Configuration();
        config.Delimiter = ";";
        config.Encoding = Encoding.UTF8;
    }

    // public static List<T> LoadCSVDataFromFile<T>(string filePath) {
    //     using (TextReader reader = new StreamReader(filePath)) {
    //         using (CsvReader csvReader = new CsvReader(reader, config)) {
    //             return csvReader.GetRecords<T>().ToList();
    //         }
    //     }
    // }

    public static List<T> LoadCSVDataFromTestAsset<T>(TextAsset csvFile) {
        using (TextReader reader = new StringReader(csvFile.text)) {
            using (CsvReader csvReader = new CsvReader(reader, config)) {
                return csvReader.GetRecords<T>().ToList();
            }
        }
    }
}
