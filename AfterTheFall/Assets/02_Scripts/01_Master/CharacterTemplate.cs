﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Template for a character, as defined in csv file
/// </summary>
[System.Serializable]
public class CharacterTemplate
{
    // properties for csv parsing
    // Have to be the same name as in the header row in the csv file
    public string Name { get; set; }
    public string ID { get; set; }
    public int Mv { get; set; }
    public int Acc { get; set; }
    public int Def { get; set; }
    public int Sz { get; set; }
    public int HP { get; set; }
    public int Sv { get; set; }
    public int Sq { get; set; }
    public string Abilities { get; set; }
    public string Keywords { get; set; }
}
