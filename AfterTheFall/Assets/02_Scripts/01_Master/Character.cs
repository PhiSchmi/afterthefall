﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Character", menuName = "Character")]
public class Character: Unit
{
    public string characterName;

    [SerializeField]
    private Unit characterClass;
    public string classID;
    public int level = 1;

    [Header ("Status")]
    public int maxArmor;
    public int currentArmor;
    public int maxHealth;
    public int currentHealth;

    [Header("Resistances")]
    public float bleedRes = 0.0f;
    public float fireRes = 0.0f;
    public float corrRes = 0.0f;
    public float poisRes = 0.0f;

    [Header("Equipment")]
    public List<List<Weapon>> WeaponSets;
    public Armor armor;
    public Mount mount;
    public Pet pet;

    [Header("CombatEffects")]
    public List<ConditionEnum> conditions;
    public List<BlessingEnum> blessings;

    public void ImplementUnit()
    {
        movementValue = characterClass.movementValue;

        if(mount != null)
        {
            movementValue = mount.newMovementValue;
        }

        sizeValue = characterClass.sizeValue;
        hands = characterClass.hands;
        versatility = characterClass.versatility;
        command = characterClass.command;
        meleeBaseDamage = characterClass.meleeBaseDamage;

        maxArmor = characterClass.armorValue + armor.armorBonus;
        currentArmor = maxArmor;
        maxHealth = characterClass.healthValue + armor.healthBonus;
        currentHealth = maxHealth;

        perception = characterClass.perception;
        resilience = characterClass.resilience;
        agility = characterClass.agility;
        strength = characterClass.strength;
        survival = characterClass.survival;
        smarts = characterClass.smarts;

        for(int i =0; i < characterClass.abilities.Count; i++)
        {
            abilities.Add(characterClass.abilities[i]);
        }

    }
}
