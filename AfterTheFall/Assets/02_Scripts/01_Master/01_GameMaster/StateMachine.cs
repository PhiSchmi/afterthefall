﻿using UnityEngine;

public class StateMachine
{
    private IState currentState;

    private IState previousState;

    public void ChangeState(IState newState)
    {
        if(currentState != null)
        {
            this.currentState.Exit();
        }

        this.previousState = currentState;

        this.currentState = newState;
        this.currentState.Enter();

        Debug.Log("State changed from " + previousState + " to " + currentState + ".");
    }

    public void ExecuteStateUpdate()
    {
        if (this.currentState != null)
        {
            this.currentState.Execute();
        }

    }

    public void SwitchToPrevious()
    {
        this.currentState.Exit();
        this.currentState = previousState;
        this.currentState.Enter();
        Debug.Log("State changed from " + previousState + " to " + currentState + ".");
    }
}
