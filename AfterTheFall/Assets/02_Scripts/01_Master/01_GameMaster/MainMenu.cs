﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void BTN_PlayGame()
    {
        SceneManager.LoadScene("Battle");
    }

    public void BTN_OpenWBCreator()
    {
        SceneManager.LoadScene("WbC");
    }

    public void BTN_Rules()
    {
        SceneManager.LoadScene("Rules");
    }

    public void BTN_Credits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void BTN_StartMenu()
    {
        SceneManager.LoadScene("01_StartMenu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
