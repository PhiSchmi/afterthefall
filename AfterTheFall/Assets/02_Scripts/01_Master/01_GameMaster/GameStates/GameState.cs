﻿public class GameState
{
    private StateMachine gameState = new StateMachine();

    private void Awake()
    {
        gameState.ChangeState(new G_Init());
    }

    void InGame()
    {
        gameState.ChangeState(new G_InGame());
    }

    private void QuitGame()
    {
        gameState.ChangeState(new G_Quit());
    }
}