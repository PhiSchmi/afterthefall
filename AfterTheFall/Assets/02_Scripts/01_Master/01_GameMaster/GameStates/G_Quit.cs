﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class G_Quit : IState
{
    public void Enter()
    {
        Debug.Log("GameState: QuitGame");

        Application.Quit();
    }

    public void Execute()
    {

    }

    public void Exit()
    {

    }
}
