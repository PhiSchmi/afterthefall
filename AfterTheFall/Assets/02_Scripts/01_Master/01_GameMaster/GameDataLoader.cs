﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Component that loads game data from csv files before game start.
/// Implements singleton pattern in the Instance property
/// </summary>
public class GameDataLoader : MonoBehaviour
{
    [SerializeField] private TextAsset characterFile;

    public List<CharacterTemplate> CharacterTemplates {get; private set;}

    public static GameDataLoader Instance {get; private set;}

    public void OnEnable() {
        if(Instance != null) {
            Destroy(Instance);
        }

        Instance = this;

        CharacterTemplates = CSVUtils.LoadCSVDataFromTestAsset<CharacterTemplate>(characterFile).ToList();
        Debug.Log( $"Loaded {CharacterTemplates.Count} character templates");
    }

    public CharacterTemplate GetCharacterTemplateByID(string characterID) {
        return CharacterTemplates.First((CharacterTemplate c) => c.ID == characterID);
    }
}
