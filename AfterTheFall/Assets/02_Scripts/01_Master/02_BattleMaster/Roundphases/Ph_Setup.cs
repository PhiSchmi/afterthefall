﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ph_Setup : IState
{
    private BattleMaster battleMaster;

    public Ph_Setup(BattleMaster master)
    {
        battleMaster = master;
    }

    public void Enter()
    {
        battleMaster.roundPhase = "Setup Phase";
    }

    public void Execute()
    {

    }

    public void Exit()
    {

    }
}
