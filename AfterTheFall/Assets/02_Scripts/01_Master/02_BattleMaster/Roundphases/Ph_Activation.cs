﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ph_Activation : IState
{
    private BattleMaster master;

    public Ph_Activation(BattleMaster master)
    {
        this.master = master;
    }

    public void Enter()
    {
        master.roundPhase = "Activation Phase";

        //Set Startplayer from initiativeOrder
        master.activePlayer = master.initiativeOrder[0];
        SwitchTargetSelection();
        //Open Activation Window
    }

    private void SwitchTargetSelection()
    {
        master.enemyTargets.Clear();

        for (int i = 0; i < master.players.Count; i++)
        {
            if (master.players[i].tag != master.activePlayer.tag)
            {
                for (int j = 0; j < master.players[i].warbandCharacters.Count; j++)
                {
                    master.enemyTargets.Add(master.players[i].warbandCharacters[j]);
                }
            }
        }
    }

    public void Execute()
    {
        //Switch to next active player
        master.initiativeOrder.RemoveAt(0);
        master.initiativeOrder.Add(master.activePlayer);
        master.activePlayer = master.initiativeOrder[0];

        SwitchTargetSelection();
    }

    public void Exit()
    {
        //Close Activation Window
    }
}
