﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ph_Maintenance : IState
{
    private BattleMaster master;

    public Ph_Maintenance(BattleMaster master)
    {
        this.master = master;
    }

    public void Enter()
    {
        master.roundPhase = "Maintenance Phase";
        master.battleRound += 1;
    }

    public void Execute()
    {

    }

    public void Exit()
    {

    }
}
