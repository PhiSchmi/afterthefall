﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ph_Initiative : IState
{
    private BattleMaster master;

    public Ph_Initiative(BattleMaster master)
    {
        this.master = master;
    }

    public void Enter()
    {
        //Open Initiative Window

        master.roundPhase = "Initiative Phase";

        for (int i = 0; i < master.players.Count; i++)
        {
            //InitiativeRoll
            master.players[i].initative = Random.Range(1, 100);
            master.initiativeOrder.Add(master.players[i]);
        }

        master.initiativeOrder.Sort(SortByInitiative);

        //Display Result in InitiativeWindow
    }

    private int SortByInitiative(Warband a, Warband b)
    {
        if(a.initative<b.initative)
        {
            return -1;
        }
        else if(a.initative>b.initative)
        {
            return 1;
        }
        return 0;
    }

    public void Execute()
    {

    }

    public void Exit()
    {
        //Close Initiative Window
    }
}
