﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ph_End : IState
{
    private BattleMaster master;

    public Ph_End(BattleMaster battleMaster)
    {
        master = battleMaster;
    }

    public void Enter()
    {
        //Open Monster Selection

        master.roundPhase = "End Phase";
        master.initiativeOrder.Clear();

        for (int i = 0; i<master.players.Count;i++)
        {
            master.players[i].ammo += Random.Range(1, 3);

            //for(int j=0; i < master.players[i].warbandCharacters.Count;j++)
            //{
            //    //for(int k = 0; k < master.players[i].warbandCharacters[j].abilities.Count; k++)
            //    //{
            //    //    if(master.players[i].warbandCharacters[j].abilities[k] == "Scavenger")
            //    //    {
            //    //        master.players[i].ammo += 1;
            //    //    }
            //    //}
            //}
        }
    }

    public void Execute()
    {
        //Close Monster Selection
        //Open Score Screen
    }

    public void Exit()
    {
        //Close Score Screen
    }
}