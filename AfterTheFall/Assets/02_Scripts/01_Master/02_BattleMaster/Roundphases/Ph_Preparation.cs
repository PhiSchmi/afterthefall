﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ph_Preparation : IState
{
    private BattleMaster master;

    private UI_Battle UI;

    public Ph_Preparation(BattleMaster master)
    {
        this.master = master;
    }

    public void Enter()
    {
        master.roundPhase = "Preparation Phase";
        //Loop through InitiativeOrder
        //Display Deployment Window of active player
    }

    public void Execute()
    {
        //repeat for next player
    }

    public void Exit()
    {
        //close Deployment Window
    }
}
