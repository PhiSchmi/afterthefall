﻿using UnityEngine;

public class MockUpWeapon : MonoBehaviour
{
   [System.Serializable]
     public class Attack
    {
        public string name;
        public string description;
        public Sprite icon;

        public float damage;
        public float critDmgMultiplier = 2.0f;
        public float range;
        public int rate = 1;
        public int aPR;
        public float mod;

        public int ammoCost;

        public string damageType;

        public bool melee;

        public string[] abilities;

    }

    public new string name;

    public Attack [] attacks;
    public Attack combo;

    public string[] abilities;

    public float resi_Armor;
    public float resi_Health;
}
