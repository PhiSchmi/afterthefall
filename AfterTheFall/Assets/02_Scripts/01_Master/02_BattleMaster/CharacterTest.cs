﻿using System.Collections.Generic;
using UnityEngine;

public class CharacterTest : MonoBehaviour
{
    private StateMachine characterStates = new StateMachine();

    public new string name;
    public string state;

    public Sprite icon;

    public string characterClass;
    public int classLevel;

    [System.Serializable]
    public class SpecialActions
    {
        public string name;
        public string description;
        public Sprite icon;

        public float damage;
        public float range;
        public float rate;
        public float mod;

        public int specialCost;

        public string damageType;

        public bool melee;
    }

    public List<SpecialActions> specialActions;

    [System.Serializable]
    public class Conditions
    {
        public string name;

        public float damage;
        public float range;
        public float rate;
        public float mod;

        public int specialCost;

        public string damageType;

        public bool melee;

    }

    public Conditions conditions = new Conditions();

    [System.Serializable]
    public class Blessings
    {
        public bool inspired;
        public bool aiming;

    }

    public Blessings blessings = new Blessings();

    public int move;
    public int size;
    public int salvaging;
    public int resilience;
    public int agility;
    public int strength;
    public int willPower;
    public int command = 1;

    public float accuracy;
    public float maxArmor;
    public float maxHealth;

    public float currentArmor;
    public float currentHealth;

    public List<string> abilities;


    public string[] keywords;

    public List<MockUpWeapon> activeWeapons;
    public List <MockUpWeapon> Set1;
    public List<MockUpWeapon> Set2;

    public MockUpArmor armor;

    private void Awake()
    {
        InReserve();

        if (armor != null)
        {
            maxArmor = maxArmor + armor.armorBonus;
            maxHealth = maxHealth + armor.healthBonus;
        }
        else
        {
            Debug.Log(name + " doesn't wear armor!");
        }

        if (Set1 != null)
        {
            activeWeapons = Set1;
        }
        else if (Set2 != null)
        {
            activeWeapons = Set2;
        }
        else
        {
            Debug.LogWarning(this.name + " is unarmed!");
        }

        if (armor != null)
        {
            for (int i = 0; i < armor.abilities.Length; i++)
            {
                abilities.Add(armor.abilities[i]);

                //Remove doubles
            }
        }
        currentArmor = maxArmor;
        currentHealth = maxHealth;
    }

    private void ApplyAbilities()
    {
        
        //send string to AbilityInterfaceManager
    }

    public void SwapWeapons()
    {
        if (activeWeapons == Set1 && Set2 != null)
        {
            activeWeapons = Set2;
        }
        else if (activeWeapons == Set2 && Set1 != null)
        {
            activeWeapons = Set1;
        }
        else
        {
            return;
        }
    }

    public void Aim()
    {
        blessings.aiming = true;
    }

    public void InReserve()
    {
        characterStates.ChangeState(new Ch_InReserve(this));
    }

    public void IsWaiting()
    {
        characterStates.ChangeState(new Ch_Waiting(this));
    }

    public void SetActive()
    {
        characterStates.ChangeState(new Ch_Active(this));
    }

    public void GoOnSentry()
    {
        characterStates.ChangeState(new Ch_Sentry(this));
    }

    public void EndActivation()
    {
        characterStates.ChangeState(new Ch_Inactive(this));
    }

    public void Injured()
    {
        characterStates.ChangeState(new Ch_Injured(this));
    }

    public void Interrupted()
    {
        characterStates.ChangeState(new Ch_Interrupted(this));
    }

}
