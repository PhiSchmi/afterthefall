﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warband : MonoBehaviour
{
    public List<CharacterTest> warbandCharacters;

    public new string name;

    public int ammo;

    
    public int initative;

    [HideInInspector]
    public float powerLevel;

}
