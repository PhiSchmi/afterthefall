﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionController : MonoBehaviour
{

    private BattleMaster master;

    public enum SelectionType { Activation, Deployment, Squad, Target }
    public SelectionType type;

    public List<CharacterTest> selectionList;

    private void Awake()
    {
        master = Object.FindObjectOfType<BattleMaster>();
    }


    public void BTN_Confirm()
    {
        for (int i = 0; i < selectionList.Count; i++)
        {
            if (type == SelectionType.Activation)
            {
                //Display active Character
                master.activeCharacter = selectionList[i];
            }
            else if (type == SelectionType.Deployment)
            {
                //Display Deployment Message
                master.waitingList.Add(selectionList[i]);
            }
            else if(type == SelectionType.Squad)
            {
                //Display Squad in a new ActivationSelection
                master.squad.Add(selectionList[i]);
            }
            else if(type == SelectionType.Target)
            {
                //perform action with targets
                master.targetCharacters.Add(selectionList[i]);
            }
        }

        selectionList.Clear();
    }

    public void BTN_Cancel()
    {
        selectionList.Clear();
    }

}
