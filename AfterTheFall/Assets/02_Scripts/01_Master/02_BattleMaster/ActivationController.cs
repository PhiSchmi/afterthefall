﻿using UnityEngine;

[RequireComponent(typeof(BattleMaster))]
public class ActivationController : MonoBehaviour
{

    BattleMaster master;
    UI_Battle battleUI;

    public float cover;
    float coverMod;
    public float range;

    float sizeMod = 1.0f;

    float resistance = 1.0f;

    float resi_weakpoint = 1.25f;
    float resi_0 = 1.0f;
    float resi_25 = 0.75f;
    float resi_50 = 0.5f;

    private void Awake()
    {
        master = Object.FindObjectOfType<BattleMaster>();
        battleUI = Object.FindObjectOfType<UI_Battle>();
    }

    public void Aim()
    {
        master.activeCharacter.blessings.aiming = true;
    }

    public void Sentry()
    {
        master.activeCharacter.GoOnSentry();
        EndActivation();
    }

    public void Loot()
    {

    }

    public void SetFire()
    {

    }

    public void SwitchWeaponSet()
    {
        //Muss auf CharacterTest noch gelöscht werden
        if (master.activeCharacter.activeWeapons == master.activeCharacter.Set1 && master.activeCharacter.Set2 != null)
        {
            master.activeCharacter.activeWeapons = master.activeCharacter.Set2;
            battleUI.DisplayAttackButtons();
        }
        else if (master.activeCharacter.activeWeapons == master.activeCharacter.Set2 && master.activeCharacter.Set1 != null)
        {
            master.activeCharacter.activeWeapons = master.activeCharacter.Set1;
            battleUI.DisplayAttackButtons();
        }
        else
        {
            return;
        }
    }

    public void StandardAttack()
    {
        for(int i = 0; i < master.activeCharacter.activeWeapons.Count; i++)
        {
            Attack(i, 0);
        }
    }

    public void SpecialAttack(int i, int j)
    {
        Attack(i, j);
    }

    public void SpecialAction(int index)
    {
        //Execute SpecialAction
    }

    private void Attack(int i, int j)
    {
        CheckResistance(master.activeCharacter.activeWeapons[i].attacks[j].damageType);

        if (range != 0.0f || master.activeCharacter.activeWeapons[i].attacks[j].melee)
        {
            for (int k = 0; k < master.activeCharacter.activeWeapons[i].attacks[j].rate; k++)
            {
                if (range <= master.activeCharacter.activeWeapons[i].attacks[j].range)
                {
                    float accuracy = 0.01f * (master.activeCharacter.accuracy + master.activeCharacter.activeWeapons[i].attacks[j].mod - 2 * range) * (master.activeCharacter.currentHealth / master.activeCharacter.maxHealth);

                    if (master.activeCharacter.blessings.aiming)
                    {
                        accuracy += 0.25f;
                    }

                    Debug.Log("Accuracy: " + accuracy * 100 + "%");

                    int critRoll = Random.Range(1, 100);
                    float failChance = 5 - (100 * accuracy - master.activeCharacter.armor.criticalResistance) / 20;

                    if (failChance <= 0)
                    {
                        failChance = 0;
                    }

                    Debug.Log("FailChance: " + failChance + "%");

                    float critChance = 95 - (100 * accuracy - master.targetCharacters[0].armor.criticalResistance) / 3;
                    float critChanceText = 100 - critChance;
                    Debug.Log("CriticalChance: " + critChanceText + "%");

                    float damageOutput = new float();
                    float damage = new float();

                    Debug.Log("DamageRatio: " + master.activeCharacter.activeWeapons[i].attacks[j].damage * accuracy * resistance * (1 - 0.1f * cover) + "-" + master.activeCharacter.activeWeapons[i].attacks[j].damage * resistance * (1 - 0.1f * cover));

                    if (critRoll <= failChance)
                    {
                        Debug.Log("MISS");
                        damageOutput = 0;
                    }
                    else if (critRoll >= critChance)
                    {
                        Debug.Log("!!!CRITICAL!!!");
                        damageOutput = Random.Range(master.activeCharacter.activeWeapons[i].attacks[j].damage * accuracy * resistance * master.activeCharacter.activeWeapons[i].attacks[j].critDmgMultiplier, master.activeCharacter.activeWeapons[i].attacks[j].damage * resistance * master.activeCharacter.activeWeapons[i].attacks[j].critDmgMultiplier);
                    }
                    else
                    {
                        damageOutput = Random.Range(master.activeCharacter.activeWeapons[i].attacks[j].damage * accuracy * resistance, master.activeCharacter.activeWeapons[i].attacks[j].damage * resistance);
                    }

                    //Calculate the Cover Modifier to the incoming damage
                    //Cover can never be negative
                    //the larger you are the less cover you get
                    float coverSizeComparison = cover - master.targetCharacters[0].size;
                    if (coverSizeComparison <= 0.0f)
                    {
                        coverSizeComparison = 0.0f;
                    }

                    coverMod = 1 - 0.1f * coverSizeComparison;

                    //Modify damage through melee attacks due to size comparison
                    //for each size the attacker is larger the damage is raised by 10%
                    //for each size the attacker is smaller the damage is reduced by 10%
                    if (master.activeCharacter.activeWeapons[i].attacks[j].melee)
                    {
                        sizeMod = 1 + 0.1f * (master.activeCharacter.size - master.targetCharacters[0].size);
                    }
                    else
                    {
                        sizeMod = 1.0f;
                    }

                    damage = damageOutput * coverMod * sizeMod;

                    DamageTarget(damage);
                }
                else
                {
                    Debug.Log(master.activeCharacter.activeWeapons[i].attacks[j].name + " is out of range.");
                }
            }
        }
        else
        {
            Debug.Log("Can't use " + master.activeCharacter.activeWeapons[i].attacks[j].name + " in melee!");
        }
        master.activeCharacter.blessings.aiming = false;

    }

    void CheckResistance(string damageType)
    {

        int i = 0;

        if (damageType == "light")
        {
            if (master.targetCharacters[i].armor != null)
            {

                if (master.targetCharacters[i].armor.armorType == MockUpArmor.Type.Light)
                {
                    resistance = resi_weakpoint;
                }
                else if (master.targetCharacters[i].armor.armorType == MockUpArmor.Type.Medium)
                {
                    resistance = resi_25;
                }
                else if (master.targetCharacters[i].armor.armorType == MockUpArmor.Type.Heavy)
                {
                    resistance = resi_50;
                }
                else
                {
                    Debug.LogWarning("Couldn't find " + master.targetCharacters[i].name + "'s armor type!");
                }
            }
            else
            {
                Debug.LogWarning(master.targetCharacters[i].name + " wears no armor!");
            }
        }
        else if (damageType == "medium")
        {
            if (master.targetCharacters[i].armor != null)
            {

                if (master.targetCharacters[i].armor.armorType == MockUpArmor.Type.Light)
                {
                    resistance = resi_0;
                }
                else if (master.targetCharacters[i].armor.armorType == MockUpArmor.Type.Medium)
                {
                    resistance = resi_weakpoint;
                }
                else if (master.targetCharacters[i].armor.armorType == MockUpArmor.Type.Heavy)
                {
                    resistance = resi_25;
                }
                else
                {
                    Debug.LogWarning("Couldn't find " + master.targetCharacters[i].name + "'s armor type!");
                }
            }
            else
            {
                Debug.LogWarning(master.targetCharacters[0].name + " wears no armor!");
            }
        }
        else if (damageType == "heavy")
        {

            if (master.targetCharacters[0].armor != null)
            {
                if (master.targetCharacters[i].armor.armorType == MockUpArmor.Type.Light)
                {
                    resistance = resi_50;
                }
                else if (master.targetCharacters[i].armor.armorType == MockUpArmor.Type.Medium)
                {
                    resistance = resi_25;
                }
                else if (master.targetCharacters[i].armor.armorType == MockUpArmor.Type.Heavy)
                {
                    resistance = resi_weakpoint;
                }
                else
                {
                    Debug.LogWarning("Couldn't find " + master.targetCharacters[i].name + "'s armor type!");
                }
            }
            else
            {
                Debug.LogWarning(master.targetCharacters[0].name + " wears no armor!");
            }
        }
        else if (damageType == "energy")
        {
            if (master.targetCharacters[0].armor != null)
            {

                if (master.targetCharacters[i].armor.armorType == MockUpArmor.Type.Light)
                {
                    resistance = resi_0;
                }
                else if (master.targetCharacters[i].armor.armorType == MockUpArmor.Type.Medium)
                {
                    resistance = resi_0;
                }
                else if (master.targetCharacters[i].armor.armorType == MockUpArmor.Type.Heavy)
                {
                    resistance = resi_0;
                }
                else
                {
                    Debug.LogWarning("Couldn't find " + master.targetCharacters[i].name + "'s armor type!");
                }
            }
            Debug.LogWarning(master.targetCharacters[0].name + " wears no armor!");
        }
        else if (damageType == "ignores")
        {
            if (master.targetCharacters[0].armor != null)
            {
                resistance = 1.0f;
            }
            else
            {
                Debug.LogWarning(master.targetCharacters[0].name + " wears no armor!");
            }
        }
        else
        {
            Debug.LogWarning("WARNING: Missing or misspelled damageType!");
        }
    }

    void DamageTarget(float damage)
    {

        if (master.targetCharacters[0].currentArmor > 0)
        {
            float resi_Armor = new float();

            for (int i = 0; i < master.targetCharacters[0].activeWeapons.Count; i++)
            {
                resi_Armor += master.targetCharacters[0].activeWeapons[i].resi_Armor;
            }

            float shieldedDamageArmor = damage * (1 - (0.01f * resi_Armor)) - master.targetCharacters[0].armor.damageReduction;

            Debug.Log("ShieldedDamageArmor: " + shieldedDamageArmor);

            float previousArmor = master.targetCharacters[0].currentArmor;
            master.targetCharacters[0].currentArmor -= shieldedDamageArmor;

            if (master.targetCharacters[0].currentArmor <= 0)
            {
                master.targetCharacters[0].currentArmor = 0;
                float overKill = shieldedDamageArmor - previousArmor;

                DamageTarget(overKill);
            }
        }
        else
        {
            float resi_Health = new float();

            for (int i = 0; i < master.targetCharacters[0].activeWeapons.Count; i++)
            {
                resi_Health += master.targetCharacters[0].activeWeapons[i].resi_Health;
            }

            float shieldedDamageHealth = damage * (1 - (0.01f * resi_Health));

            Debug.Log("ShieldedDamageHealth: " + shieldedDamageHealth);

            master.targetCharacters[0].currentHealth -= shieldedDamageHealth;

            if (master.targetCharacters[0].currentHealth <= 0)
            {
                master.targetCharacters[0].currentHealth = 0;
            }
        }
    }

    public void StartActivation()
    {

    }

    public void EndActivation()
    {
        master.activeCharacter.EndActivation();

        if(master.squad[0] != null)
        {
            master.activeCharacter = master.squad[0];
            master.squad.Remove(master.activeCharacter);
        }
        else
        {
            //Switch to next player
        }

        
    }



}
