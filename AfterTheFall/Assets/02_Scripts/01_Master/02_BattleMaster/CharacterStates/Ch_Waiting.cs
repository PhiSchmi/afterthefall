﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ch_Waiting : IState
{
    private CharacterTest character;

    private BattleMaster master;

    public Ch_Waiting(CharacterTest character)
    {
        this.character = character;
        master = Object.FindObjectOfType<BattleMaster>();
    }

    public void Enter()
    {
        character.state = "Waiting";
        master.waitingList.Add(character);
    }

    public void Execute()
    {

    }

    public void Exit()
    {
        master.waitingList.Remove(character);
    }
}
