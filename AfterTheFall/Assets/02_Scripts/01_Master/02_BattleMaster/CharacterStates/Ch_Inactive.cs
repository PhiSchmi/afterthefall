﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ch_Inactive : IState
{
    private CharacterTest character;

    public Ch_Inactive(CharacterTest character)
    {
        this.character = character;
    }

    public void Enter()
    {
        character.state = "Inactive";
    }

    public void Execute()
    {

    }

    public void Exit()
    {

    }
}
