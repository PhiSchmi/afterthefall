﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ch_Sentry : IState
{
    private CharacterTest character;

    private BattleMaster master;

    public Ch_Sentry(CharacterTest character)
    {
        this.character = character;
        master = Object.FindObjectOfType<BattleMaster>();
    }

    public void Enter()
    {
        character.state = "Sentry";
        master.onSentry.Add(character);
        
    }

    public void Execute()
    {

    }

    public void Exit()
    {
        master.onSentry.Remove(character);
    }
}
