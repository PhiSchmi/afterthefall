﻿using UnityEngine;

public class Ch_InReserve : IState
{
    private CharacterTest character;


    public Ch_InReserve(CharacterTest character)
    {
        this.character = character;
    }

    public void Enter()
    {
        character.state = "InReserve";
    }

    public void Execute()
    {

    }

    public void Exit()
    {

    }
}
