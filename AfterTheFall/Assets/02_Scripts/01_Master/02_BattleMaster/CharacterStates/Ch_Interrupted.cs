﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ch_Interrupted : IState
{
    private CharacterTest character;

    private BattleMaster master;

    public Ch_Interrupted(CharacterTest character)
    {
        this.character = character;
        master = Object.FindObjectOfType<BattleMaster>();
    }

    public void Enter()
    {
        character.state = "Interrupted";
        master.interruptedCharacters.Add(character);
    }

    public void Execute()
    {

    }

    public void Exit()
    {
        master.interruptedCharacters.Remove(character);
    }
}
