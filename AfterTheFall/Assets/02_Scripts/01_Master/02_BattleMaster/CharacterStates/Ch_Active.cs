﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ch_Active : IState
{
    private CharacterTest character;

    private BattleMaster master;

    public Ch_Active(CharacterTest character)
    {
        this.character = character;
        master = Object.FindObjectOfType<BattleMaster>();
    }

    public void Enter()
    {
        character.state = "Active";
        master.activeCharacter = character;

        Debug.Log(character.name + " is now active.");
    }

    public void Execute()
    {

    }

    public void Exit()
    {
        
    }
}
