﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ch_Injured : IState
{
    private CharacterTest character;

    private BattleMaster master;

    public Ch_Injured(CharacterTest character)
    {
        this.character = character;
        master = Object.FindObjectOfType<BattleMaster>();
    }

    public void Enter()
    {
        character.state = "Injured";
        //TextMessage: Remove model from play replace it with LootArea
        master.lootAreas.Add(new MockUpLootArea());
    }

    public void Execute()
    {

    }

    public void Exit()
    {

    }
}
