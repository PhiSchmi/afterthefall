﻿using UnityEngine.UI;
using UnityEngine;

public class UI_StatCard : MonoBehaviour
{
    public CharacterTest character;
    private SelectionController selection;


    public Text nameText;
    public Sprite icon;
    public Text movementValueText;
    public Text sizeValueText;
    public Text keywordsText;

    public Image armorPoints;
    public Text armorText;
    public Image healthPoints;
    public Text healthText;

    private void Awake()
    {
        selection = Object.FindObjectOfType<SelectionController>();

        //Display Name
        nameText.text = character.name;

        //Display Icon
        icon = character.icon;

        //Display MovementValue
        if (movementValueText != null)
        {
            movementValueText.text = character.move.ToString();
        }

        //Display SizeValue
        if (sizeValueText != null)
        {
            sizeValueText.text = character.size.ToString();
        }

        //Display Keywords
        if (keywordsText != null)
        {
            for (int i = 0; i < character.keywords.Length; i++)
            {
                keywordsText.text += character.keywords[i];

                if (i > character.keywords.Length)
                {
                    keywordsText.text += ", ";
                }
            }
        }
    }

    //private void Update()
    //{
    //    if (this != null)
    //    {
    //        armorPoints.fillAmount = character.currentArmor / character.maxArmor;
    //        armorText.text = character.currentArmor.ToString();
    //        healthPoints.fillAmount = character.currentHealth / character.maxHealth;
    //        healthText.text = character.currentHealth.ToString();
    //    }
    //}

    public void BTN_OnClick()
    {
        for (int i=0; i<selection.selectionList.Count;i++)
        {
            if (selection.selectionList[i] = character)
            {
                selection.selectionList.Remove(selection.selectionList[i]);
            }
        }

        selection.selectionList.Add(character);

        
    }
}