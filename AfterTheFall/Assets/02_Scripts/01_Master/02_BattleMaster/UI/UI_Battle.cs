﻿using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UI_Battle : MonoBehaviour
{
    BattleMaster master;


    public Text messageBoxText;
    public Transform MessageBox;
    public Transform window;

    [System.Serializable]
    public class ActiveWarband
    {
        public Text name;
        public Text ammo;
    }

    [System.Serializable]
    public class ActiveCharacter
    {
        public Text name;
        public Sprite icon;
        public Text movementValue;
        public Text sizeValue;

        public Image armorPoints;
        public Text armorText;
        public Image healthPoints;
        public Text healthText;

        public Text keywords;

        public Text abilities;

        public List<UI_ActionButton> actionButtons;
        public GridLayoutGroup attackGrid;
        public GridLayoutGroup specActionGrid;
    }

    public ActiveCharacter activeCharacter = new ActiveCharacter();
    public ActiveWarband warband = new ActiveWarband();

    public GridLayoutGroup selectionGrid;
    public UI_StatCard prfb_SelectionIcon;
    public UI_StatCard prfb_TargetIcon;
    public UI_ActionButton prfb_ActionButton;

    private void Start()
    {
        master = Object.FindObjectOfType<BattleMaster>();
        DisplayAttackButtons();

    }

    public void DisplayCharacterSelection(CharacterTest characterData, UI_StatCard targetObject)
    {
        Instantiate(targetObject.gameObject, window);
        targetObject.gameObject.transform.parent = window;
        targetObject.character = characterData;
    }

    public void DisplayActiveCharacter()
    {
        activeCharacter.name.text = master.activeCharacter.name;
        activeCharacter.icon = master.activeCharacter.icon;
        activeCharacter.movementValue.text = master.activeCharacter.move.ToString();
        activeCharacter.sizeValue.text = master.activeCharacter.size.ToString();

        for (int i = 0; i < master.activeCharacter.keywords.Length; i++)
        {
            activeCharacter.keywords.text += master.activeCharacter.keywords[i];

            if (i < master.activeCharacter.keywords.Length)
            {
                activeCharacter.keywords.text += ", ";
            }
        }

        for (int i = 0; i < master.activeCharacter.abilities.Count; i++)
        {
            activeCharacter.abilities.text += master.activeCharacter.abilities[i];

            if (i < master.activeCharacter.abilities.Count)
            {
                activeCharacter.abilities.text += ", ";
            }
        }

        UpdateHealthStatus(master.activeCharacter);

        DisplayAttackButtons();
        DisplaySpecialAttackButtons();

    }

    public void DisplayAttackButtons()
    {
        //Clear AttackGrid
        for (int i = 1; i < activeCharacter.attackGrid.transform.childCount; i++)
        {
            UI_ActionButton oldButton = activeCharacter.attackGrid.transform.GetChild(i).GetComponent<UI_ActionButton>(); 

            Destroy(oldButton.gameObject);
        }

        //Populate AttackGrid
        for (int i = 0; i < master.activeCharacter.activeWeapons.Count; i++)
        {
            for (int j = 1; j < master.activeCharacter.activeWeapons[i].attacks.Length; j++)
            {
                Debug.Log("ActionButton " + master.activeCharacter.activeWeapons[i].attacks[j].name + " created.");
                UI_ActionButton newButton = prfb_ActionButton;
                Instantiate(newButton, activeCharacter.attackGrid.transform);

                newButton.type = UI_ActionButton.ButtonType.SpecialAttack;

                if (master.activeCharacter.activeWeapons[i].attacks[j].icon != null)
                {
                    newButton.actionIcon = master.activeCharacter.activeWeapons[i].attacks[j].icon;
                }

                newButton.GetComponent<Image>().sprite = newButton.actionIcon;

                newButton.actionName = master.activeCharacter.activeWeapons[i].attacks[j].name;
                newButton.descriptionText = master.activeCharacter.activeWeapons[i].attacks[j].description;
                newButton.dedicatedWeapon = i;
                newButton.dedicatedAction = j;
            }
        }    
    }

    public void DisplaySpecialAttackButtons()
    {
        //Clear SpecialActionGrid
        for (int i=0; i<activeCharacter.specActionGrid.transform.childCount; i++)
        {
            UI_ActionButton oldButton = activeCharacter.specActionGrid.transform.GetChild(i).GetComponent<UI_ActionButton>();

            Destroy(oldButton.gameObject);
        }

        //Populate SpecialActionGrid
        for (int i = 0; i < master.activeCharacter.specialActions.Count; i++)
        {
            UI_ActionButton newButton = prfb_ActionButton;
            Instantiate(newButton, activeCharacter.specActionGrid.transform);

            newButton.type = UI_ActionButton.ButtonType.SpecialAction;

            if (master.activeCharacter.specialActions[i].icon != null)
            {
                newButton.actionIcon = master.activeCharacter.specialActions[i].icon;
            }

            newButton.GetComponent<Image>().sprite = newButton.actionIcon;

            newButton.actionName = master.activeCharacter.specialActions[i].name;
            newButton.descriptionText = master.activeCharacter.specialActions[i].description;
            newButton.specialActionIndex = i;
        }
    }

    private void UpdateHealthStatus(CharacterTest characterData)
    {
        activeCharacter.armorPoints.fillAmount = characterData.currentArmor / characterData.maxArmor;
        activeCharacter.armorText.text = characterData.currentArmor.ToString();
        activeCharacter.healthPoints.fillAmount = characterData.currentHealth / characterData.maxHealth;
        activeCharacter.healthText.text = characterData.currentHealth.ToString();
    }

    private void DisplayMessage(string text)
    {
        window.gameObject.SetActive(enabled);
        messageBoxText.text = text;
    }

    private void DisplayDeploymentSelection()
    {
        for (int i = 0; i < master.inReserve.Count; i++)
        {
            UI_StatCard newIcon = prfb_SelectionIcon;
            Instantiate(newIcon, selectionGrid.transform);

            if(master.inReserve[i].icon != null)
            {
                newIcon.icon = master.inReserve[i].icon;
            }

            newIcon.GetComponent<Image>().sprite = newIcon.icon;

            newIcon.name = master.inReserve[i].name;
            newIcon.sizeValueText.text = master.inReserve[i].size.ToString();
            newIcon.movementValueText.text = master.inReserve[i].move.ToString();
            newIcon.armorPoints.fillAmount = master.inReserve[i].currentArmor / master.inReserve[i].maxArmor;
            newIcon.armorText.text = master.inReserve[i].currentArmor.ToString();
            newIcon.healthPoints.fillAmount = master.inReserve[i].currentHealth / master.inReserve[i].maxHealth;
            newIcon.healthText.text = master.inReserve[i].currentHealth.ToString();
        }
    }

    private void DisplayTargetSelection()
    {
        for (int i = 0; i < master.enemyTargets.Count; i++)
        {

            //create a Button in Window
            //Display target.name
            //Display target.icon
            //Display target.size
            //Display target.armor
            //Display target.health
        }
    }

    private void DisplayCombatScreen()
    {
        //Display targets
        for (int i = 0; i < master.targetCharacters.Count; i++)
        {
            //Create Combat Icon in the target area of the combat screen
            //Display armor, health, icon and name
            //Display animation
            //Display damage
        }
    }

    private void DisplayActiveWarband()
    {
        warband.name.text = master.activePlayer.name;
        warband.ammo.text = master.activePlayer.ammo.ToString();
    }

    private void DisplayCharacterSelection()
    {

    }

    private void DisplaySquadSelection()
    {
        
    }

}
