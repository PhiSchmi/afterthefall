﻿using UnityEngine.UI;
using UnityEngine;

[RequireComponent(typeof(Button))]
public class UI_ActionButton : MonoBehaviour
{
    public Sprite actionIcon;
    BattleMaster master;
    ActivationController controller;

    public enum ButtonType { Aim, Sentry, Loot, SetFire, StandardAttack, SpecialAttack, SpecialAction, Switch }
    public ButtonType type;

    public string actionName;
    public string descriptionText;

    public int dedicatedWeapon;
    public int dedicatedAction;
    public int specialActionIndex;

    private void Awake()
    {
        master = Object.FindObjectOfType<BattleMaster>();
        controller = Object.FindObjectOfType<ActivationController>();
    }
  
    public void BTN_OnClick()
    {
        if (type == ButtonType.Aim)
        {
            controller.Aim();
        }
        else if (type == ButtonType.Sentry)
        {
            master.activeCharacter.GoOnSentry();
            controller.EndActivation();

        }
        else if (type == ButtonType.Loot)
        {

        }
        else if (type == ButtonType.SetFire)
        {

        }
        else if (type == ButtonType.StandardAttack)
        {
            controller.StandardAttack();
        }
        else if (type == ButtonType.SpecialAttack)
        {
            controller.SpecialAttack(dedicatedWeapon, dedicatedAction);
        }
        else if (type == ButtonType.SpecialAction)
        {
            controller.SpecialAction(specialActionIndex);
        }
        else if (type == ButtonType.Switch)
        {
            controller.SwitchWeaponSet();
        }
        else
        {
            Debug.LogError("ButtonType not found!");
        }
    }





}
