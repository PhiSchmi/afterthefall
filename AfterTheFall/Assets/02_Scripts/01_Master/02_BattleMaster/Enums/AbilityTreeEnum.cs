﻿public enum Enum_AbilityTree
{
    None,
    Agility,
    Brawl,
    Bulk,
    Cunning,
    Dream,
    Leadership,
    Perception,
    Smarts,
    Strength,
    Mutations
}


