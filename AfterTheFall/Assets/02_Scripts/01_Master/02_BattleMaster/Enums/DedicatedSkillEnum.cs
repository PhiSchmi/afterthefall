﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DedicatedSkillEnum
{
    Accuracy,
    Agility,
    Resilience,
    Smarts,
    Strength,
    Survival
}
