﻿public enum SpeciesEnum
{
    Human,
    Mutant,
    Machine,
    Synthian,
    Alien
}