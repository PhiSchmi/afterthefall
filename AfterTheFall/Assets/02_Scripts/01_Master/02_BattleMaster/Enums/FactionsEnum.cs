﻿public enum FactionsEnum
{
    General,
    Arkers,
    Collectionists,
    Horde,
    Hunes,
    Raiders,
    RoadMarshalls,
    Savages,
    Traders,
    WastelandHazards
}