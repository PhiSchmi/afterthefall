﻿public enum BlessingEnum
{
    Aiming,                 // +20% Accuracy
    Inspired,               // +10% damage and Skillchecks
    Shield,                 // ignore next suffered damage
    Tenacity,               // Extra Action
    Acceleration,           // +1 Movement
    Lucky,                  // +100 on the next Skillcheck
    Guarded                 // +25% Resistance
}
