﻿public enum ConditionEnum 
{
    //All following conditions are stackable in each EndPhase every Stack is reduced by 1
    Bleed,          //Loses X HP for every BleedIcon in beginning of activation -- LIVING only (Damage Reduced through resilience)
    Blinded,        //-10% on Attacks
    Corrosion,      //Loses X Armor for every CorrosionIcon in beginning of activation
    Burning,        
    Poisoned,       //Loses X HP if this character performs an attack  -- LIVING only (Damage Reduced through resilience)
    Stunned         //Cannot perform Attacks
    
}
