﻿using UnityEngine;
using UnityEngine.UI;

public class MockUpArmor : MonoBehaviour
{

    public new string name;
    public TextMesh description;
    public Image icon;

    public float armorBonus;
    public float healthBonus;

    public float armorResistance;
    public float healthResistance;
    public float criticalResistance;

    public float damageReduction;

    public string[] abilities;

    public enum Type { Light, Medium, Heavy}
    public Type armorType;

}
