﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ActivationController))]
public class BattleMaster : MonoBehaviour
{
    StateMachine roundphase = new StateMachine();
    UI_Battle UI;
    public Scenario scenario;
    

    public int battleRound = 0;
    public string roundPhase;

    public List<Warband> players;
    public Warband activePlayer;

    public List<Warband> initiativeOrder;


    public CharacterTest activeCharacter;

    public List<CharacterTest> waitingList;
    public List<CharacterTest> targetCharacters;
    public List<CharacterTest> interruptedCharacters;
    public List<CharacterTest> squad;
    public List<CharacterTest> squadSelection;
    public List<CharacterTest> onSentry;
    public List<CharacterTest> inReserve;
    public List<CharacterTest> deploymentSelection;
    public List<CharacterTest> enemyTargets;

    public List<MockUpLootArea> lootAreas;
    

    private void Awake()
    {
        initiativeOrder.Clear();

        roundphase.ChangeState(new Ph_Setup(this));

        Debug.Log("Search.Init");

    }

    void Start()
    {
        
    }

    void SetupSquadSelection()
    {
        for (int i = 0; i < activePlayer.warbandCharacters.Count; i++)
        {
            squadSelection.Clear();

            if (activeCharacter.keywords[5] == "COMMANDER" || activeCharacter.keywords[5] == activePlayer.warbandCharacters[i].keywords[5] || activePlayer.warbandCharacters[i].keywords[5] == "COMMANDER" || activePlayer.warbandCharacters[i].keywords[5] == "COMPANION")
            {
                if (activePlayer.warbandCharacters[i].state == "Waiting")
                {
                    squadSelection.Add(activePlayer.warbandCharacters[i]);
                }
            }
        }

        for (int i = 0; i < squadSelection.Count; i++)
        {
            UI.DisplayCharacterSelection(squadSelection[i], UI.prfb_SelectionIcon);
        }
    }

    void SetupDeploymentScreen()
    {
        deploymentSelection.Clear();

        for (int i = 0; i < activePlayer.warbandCharacters.Count; i++)
        {
            if (activePlayer.warbandCharacters[i].state == "InReserve")
            {
                deploymentSelection.Add(activePlayer.warbandCharacters[i]);
            }
        }

        for (int i = 0; i < deploymentSelection.Count; i++)
        {
            UI.DisplayCharacterSelection(deploymentSelection[i], UI.prfb_SelectionIcon);
        }
    }

    public void BTN_NextPhase()
    {
        if (roundPhase == "Setup Phase")
        {
            roundphase.ChangeState(new Ph_Maintenance(this));
        }
        else if (roundPhase == "Maintenance Phase")
        {
            roundphase.ChangeState(new Ph_Initiative(this));
        }
        else if (roundPhase == "Initiative Phase")
        {
            roundphase.ChangeState(new Ph_Preparation(this));
        }
        else if (roundPhase == "Preparation Phase")
        {
            roundphase.ChangeState(new Ph_Activation(this));
        }
        else if (roundPhase == "Activation Phase")
        {
            roundphase.ChangeState(new Ph_End(this));
        }
        else if (roundPhase == "End Phase")
        {
            roundphase.ChangeState(new Ph_Maintenance(this));
        }
        else
        {
            Debug.LogError("RoundPhase not found!");
        }
    }

    public void BTN_EndActivation()
    {
        if(roundPhase == "Activation Phase")
        {
            if(waitingList.Count != 0)
            {
                roundphase.ExecuteStateUpdate();
            }
            else
            {
                BTN_NextPhase();
            }
        }
    }

}
