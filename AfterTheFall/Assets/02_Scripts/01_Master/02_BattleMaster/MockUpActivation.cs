﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MockUpActivation : MonoBehaviour
{

    
    public CharacterTest activeModel;
    public CharacterTest targetModel;
    public MockUpLootArea targetLootArea;
    public Warband activePlayer;

    public List<Warband> players;

    public List<CharacterTest> activeModels;
    public List<CharacterTest> targetModels;
    public List<CharacterTest> charactersOnSentry;
    public List<CharacterTest> squad;
    public List<CharacterTest> squadSelection;
    public List<CharacterTest> waitingList;
    public List<CharacterTest> interrupted;


    public GameObject selectionIcon;
    public GameObject targetIcon;

    public float cover;
    float coverMod;
    public float range;
    public int weaponSpecial;
    public int attackSpecial;


    float sizeMod = 1.0f;

    float resistance = 1.0f;

    float resi_weakpoint = 1.25f;
    float resi_0 = 1.0f;
    float resi_25 = 0.75f;
    float resi_50 = 0.5f;

    bool aiming;

    UI_Battle UI;




    private void Awake()
    {
        activeModels.Clear();
        targetModels.Clear();
        charactersOnSentry.Clear();
        squad.Clear();
        waitingList.Clear();
        interrupted.Clear();
    }


    public void BTN_SwitchSet()
    {
        activeModel.SwapWeapons();
    }

    public void BTN_Aim()
    {
        activeModel.Aim();
    }

    public void BTN_StandardAttack()
    {
        
        Debug.Log(activeModel.name + " attacks " + targetModel.name + ".");

        for (int i = 0; i < activeModel.activeWeapons.Count; i++)
        {
            Attack(i, 0);
        }

        activeModel.blessings.aiming = false;
    }

    public void BTN_SpecialAttack()
    {
        Attack(weaponSpecial, attackSpecial);
    }

    public void BTN_Sentry()
    {
        activeModel.GoOnSentry();
    }

    public void BTN_ActivateOnSentry()
    {
        activeModel.Interrupted();

    }

    public void BTN_SquadSelection()
    {
        for (int i = 0; i < activePlayer.warbandCharacters.Count; i++)
        {
            squadSelection.Clear();

            if (activeModel.keywords[5] == "COMMANDER" || activeModel.keywords[5] == activePlayer.warbandCharacters[i].keywords[5] || activePlayer.warbandCharacters[i].keywords[5] == "COMMANDER" || activePlayer.warbandCharacters[i].keywords[5] == "COMPANION")
            {
                if (activePlayer.warbandCharacters[i].state == "Waiting")
                {
                    squadSelection.Add(activePlayer.warbandCharacters[i]);
                }
            }
        }

        for(int i = 0; i < squadSelection.Count; i++)
        {
            Instantiate(selectionIcon);
            UI.DisplayCharacterSelection(squadSelection[i], UI.prfb_SelectionIcon);


            //Add a button to SelectionWindow
        }

        //loop through all models of that player
        //add COMMANDER, COMPANION and Squad models to squadSelection
        //on click friendly character --> add to squadlist
        //on click chosen --> remove from squadlist
        //cmd-Counter
        //create squad_button
    }

    public void BTN_Loot()
    {
        int lootRoll = Random.Range(1, 100);
        int result = lootRoll + activeModel.salvaging;

        Debug.Log("SalvagingCheck: " + lootRoll + " + " + activeModel.salvaging);

        if (result > 99)
        {
            Debug.Log("BigLoot");
            activePlayer.ammo += Random.Range(1, 3);
            int bigLootRoll = Random.Range(1, targetLootArea.bigLoot.Count);
        }
        else if (result < 100)
        {
            Debug.Log("smallLoot");
            activePlayer.ammo += 1;
            int smallLootRoll = Random.Range(1, targetLootArea.smallLoot.Count);
        }
    }

    private void Attack(int i, int j)
    {
        CheckResistance(activeModel.activeWeapons[i].attacks[j].damageType);

        if (activeModel.activeWeapons[i].attacks[j].ammoCost > activePlayer.ammo)
        {
            Debug.Log("Not enough ammo!");
        }
        else
        {

            if (range != 0.0f || activeModel.activeWeapons[i].attacks[j].melee)
            {
                for (int k = 0; k < activeModel.activeWeapons[i].attacks[j].rate; k++)
                {
                    if (range <= activeModel.activeWeapons[i].attacks[j].range)
                    {
                        float accuracy = 0.01f * (activeModel.accuracy + activeModel.activeWeapons[i].attacks[j].mod - 2 * range) * (activeModel.currentHealth / activeModel.maxHealth);

                        if (activeModel.blessings.aiming)
                        {
                            accuracy += 0.25f;
                        }

                        Debug.Log("Accuracy: " + accuracy * 100 + "%");

                        int critRoll = Random.Range(1, 100);
                        float failChance = 5 - (100 * accuracy - targetModel.armor.criticalResistance) / 20;

                        if (failChance <= 0)
                        {
                            failChance = 0;
                        }

                        Debug.Log("FailChance: " + failChance + "%");

                        float critChance = 95 - (100 * accuracy - targetModel.armor.criticalResistance) / 3;
                        float critChanceText = 100 - critChance;
                        Debug.Log("CriticalChance: " + critChanceText + "%");

                        float damageOutput = new float();
                        float damage = new float();

                        Debug.Log("DamageRatio: " + activeModel.activeWeapons[i].attacks[j].damage * accuracy * resistance * (1 - 0.1f * cover) + "-" + activeModel.activeWeapons[i].attacks[j].damage * resistance * (1 - 0.1f * cover));

                        if (critRoll <= failChance)
                        {
                            Debug.Log("MISS");
                            damageOutput = 0;
                        }
                        else if (critRoll >= critChance)
                        {
                            Debug.Log("!!!CRITICAL!!!");
                            activePlayer.ammo += 1;
                            damageOutput = Random.Range(activeModel.activeWeapons[i].attacks[j].damage * accuracy * resistance * activeModel.activeWeapons[i].attacks[j].critDmgMultiplier, activeModel.activeWeapons[i].attacks[j].damage * resistance * activeModel.activeWeapons[i].attacks[j].critDmgMultiplier);
                        }
                        else
                        {
                            damageOutput = Random.Range(activeModel.activeWeapons[i].attacks[j].damage * accuracy * resistance, activeModel.activeWeapons[i].attacks[j].damage * resistance);
                        }

                        //Calculate the Cover Modifier to the incoming damage
                        //Cover can never be negative
                        //the larger you are the less cover you get
                        float coverSizeComparison = cover - targetModel.size;
                        if (coverSizeComparison <= 0.0f)
                        {
                            coverSizeComparison = 0.0f;
                        }

                        coverMod = 1 - 0.1f * coverSizeComparison;

                        //Modify damage through melee attacks due to size comparison
                        //for each size the attacker is larger the damage is raised by 10%
                        //for each size the attacker is smaller the damage is reduced by 10%
                        if (activeModel.activeWeapons[i].attacks[j].melee)
                        {
                            sizeMod = 1 + 0.1f * (activeModel.size - targetModel.size);
                        }
                        else
                        {
                            sizeMod = 1.0f;
                        }

                        damage = damageOutput * coverMod * sizeMod;

                        DamageTarget(damage);
                    }
                    else
                    {
                        Debug.Log(activeModel.activeWeapons[i].attacks[j].name + " is out of range.");
                    }
                }
            }
            else
            {
                Debug.Log("Can't use " + activeModel.activeWeapons[i].attacks[j].name + " in melee!");
            }
            activeModel.blessings.aiming = false;
        }
    }

    void CheckResistance(string damageType)
    {

        if (damageType == "light")
        {
            if (targetModel.armor != null)
            {

                if (targetModel.armor.armorType == MockUpArmor.Type.Light)
                {
                    resistance = resi_weakpoint;
                }
                else if (targetModel.armor.armorType == MockUpArmor.Type.Medium)
                {
                    resistance = resi_25;
                }
                else if (targetModel.armor.armorType == MockUpArmor.Type.Heavy)
                {
                    resistance = resi_50;
                }
                else
                {
                    Debug.LogWarning("Couldn't find " + targetModel.name + "'s armor type!");
                }
            }
            else
            {
                Debug.LogWarning(targetModel.name + " wears no armor!");
            }
        }
        else if (damageType == "medium")
        {
            if (targetModel.armor != null)
            {

                if (targetModel.armor.armorType == MockUpArmor.Type.Light)
                {
                    resistance = resi_0;
                }
                else if (targetModel.armor.armorType == MockUpArmor.Type.Medium)
                {
                    resistance = resi_weakpoint;
                }
                else if (targetModel.armor.armorType == MockUpArmor.Type.Heavy)
                {
                    resistance = resi_25;
                }
                else
                {
                    Debug.LogWarning("Couldn't find " + targetModel.name + "'s armor type!");
                }
            }
            else
            {
                Debug.LogWarning(targetModel.name + " wears no armor!");
            }
        }
        else if (damageType == "heavy")
        {
            if (targetModel.armor != null)
            {                
                    if (targetModel.armor.armorType == MockUpArmor.Type.Light)
                    {
                        resistance = resi_50;
                    }
                    else if (targetModel.armor.armorType == MockUpArmor.Type.Medium)
                    {
                        resistance = resi_25;
                    }
                    else if (targetModel.armor.armorType == MockUpArmor.Type.Heavy)
                    {
                        resistance = resi_weakpoint;
                    }
                    else
                    {
                        Debug.LogWarning("Couldn't find " + targetModel.name + "'s armor type!");
                    }
            }
            else
            {
                Debug.LogWarning(targetModel.name + " wears no armor!");
            }
        }
        else if (damageType == "energy")
        {
            if (targetModel.armor != null)
            {

                if (targetModel.armor.armorType == MockUpArmor.Type.Light)
                {
                    resistance = resi_0;
                }
                else if (targetModel.armor.armorType == MockUpArmor.Type.Medium)
                {
                    resistance = resi_0;
                }
                else if (targetModel.armor.armorType == MockUpArmor.Type.Heavy)
                {
                    resistance = resi_0;
                }
                else
                {
                    Debug.LogWarning("Couldn't find " + targetModel.name + "'s armor type!");
                }
            }
            Debug.LogWarning(targetModel.name + " wears no armor!");
        }
        else if (damageType == "ignores")
        {
            if (targetModel.armor != null)
            {
                resistance = 1.0f;
            }
            else
            {
                Debug.LogWarning(targetModel.name + " wears no armor!");
            }
        }
        else
        {
            Debug.LogWarning("WARNING: Missing or misspelled damageType!");
        }
    }

    void DamageTarget(float damage)
    {

        if (targetModel.currentArmor > 0)
        {
            float resi_Armor = new float();

            for (int i = 0; i < targetModel.activeWeapons.Count; i++)
            {
                resi_Armor += targetModel.activeWeapons[i].resi_Armor;
            }

            float shieldedDamageArmor = damage * (1 - (0.01f * resi_Armor)) - targetModel.armor.damageReduction;

            Debug.Log("ShieldedDamageArmor: " + shieldedDamageArmor);

            float previousArmor = targetModel.currentArmor;
            targetModel.currentArmor -= shieldedDamageArmor;

            if (targetModel.currentArmor <= 0)
            {
                targetModel.currentArmor = 0;
                float overKill = shieldedDamageArmor - previousArmor;

                DamageTarget(overKill);
            }
        }
        else
        {
            float resi_Health = new float();

            for (int i = 0; i < targetModel.activeWeapons.Count; i++)
            {
                resi_Health += targetModel.activeWeapons[i].resi_Health;
            }

            float shieldedDamageHealth = damage * (1 - (0.01f * resi_Health));

            Debug.Log("ShieldedDamageHealth: " + shieldedDamageHealth);

            targetModel.currentHealth -= shieldedDamageHealth;

            if (targetModel.currentHealth <= 0)
            {
                targetModel.currentHealth = 0;
            }
        }
    }

    public void BTN_EndActivation()
    {

    }

}
