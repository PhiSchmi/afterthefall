﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New DataLibrary", menuName = "Library/DataLibrary")]
public class DataLibrary : ScriptableObject
{    


    [System.Serializable]
    public class Equipment
    {
        

        [System.Serializable]
        public class Usable
        {
            public string name;
            public string ID;
            public string descriptions;

            [Header("Values")]

            //stuff usables do
            public List<string> abilities;
        }

        public List<Weapon> weapons = new List<Weapon>();
        public List<Armor> armors = new List<Armor>();
        public List<Usable> usables = new List<Usable>();
    }


    [System.Serializable]
    public class Mutation
    {
        public string name;
        public string ID;
        public string description;

        public int movementMod;
        public int sizeMod;
        public int accuracyMod;

        public int resilienceMod;
        public int agilityMod;
        public int strengthMod;
        public int salvagingMod;
        public int smartsMod;

        public int armorMod;
        public int healthMod;

        public enum Types { Physical, Hidden }
        public Types type;

    }

    public class Pet
    {
        public string name;
        public string ID;
        public string description;
    }

    public class Mount
    {
        public string name;
        public string ID;
        public string description;

        public enum MountType { Light, Monstrous }
        public MountType type;

    }

    public List<Unit> units = new List<Unit>();
    public Equipment equipmentContainer = new Equipment();
    public List<Pet> pets = new List<Pet>();
    public List<Mount> mounts = new List<Mount>();
    public List<Mutation> mutations = new List<Mutation>();


    public void SearchData(string ID)
    {

    }

}
