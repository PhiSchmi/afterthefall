﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Library/Equipment/Weapon")]
public class Weapon : Equipment
{
    [Range(1, 2)] public int hands;
    public int overSizeThreshold;

    [System.Serializable]
    public class Attack
    {
        public string name;
        public string description;

        public DedicatedSkillEnum dedicatedSkill;

        [Header("Values")]
        public int damage;
        public float critDmgMultiplier = 2.0f;
        public int range;
        public int rate;
        public int aPR;
        public int mod;

        public int shinyCost;

        public DamageTypeEnum damageType;

        public AttackTypeEnum attackType;

        public List<Ability> abilities;
    }

    public List<Attack> attacks = new List<Attack>();
    public Attack combo = new Attack();

    public List<Ability> abilities;
}
