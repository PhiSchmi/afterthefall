﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Armor", menuName = "Library/Equipment/Armor")]
public class Armor : Equipment
{
    [Header("Values")]
    public int armorBonus;
    public int healthBonus;
    public int sizeBonus;

    public int armorResistance;
    public int healthResistance;
    public int criticalResistance;
    public int damageReduction;

    public ArmorTypeEnum armorType;

    public List<Ability> abilities;
}
