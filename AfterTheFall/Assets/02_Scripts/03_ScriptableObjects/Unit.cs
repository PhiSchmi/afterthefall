﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Unit", menuName = "Library/Unit")]
public class Unit : ScriptableObject
{
    [Header("General")]
    public string unitName;
    public string ID;

    public FactionsEnum faction;
    public SpeciesEnum species;
    public enum UnitType { Living, NonLiving }
    public UnitType unitType;

    public string squadKeyword;

    public string description;

    [Header("Values")]
    public int movementValue;
    public int sizeValue;
    public int hands = 2;
    public int versatility = 2;     //number of WeaponSets, this unit can equip
    public int command;
    public int meleeBaseDamage;

    public int armorValue;
    public int healthValue;

    [Header("Skills")]
    public int perception;
    public int resilience;
    public int agility;
    public int strength;
    public int survival;
    public int smarts;


    public List<Ability> abilities;
    public List<Enum_AbilityTree> abilityTrees = new List<Enum_AbilityTree>();
    public List<SpecialAction> specialActions = new List<SpecialAction>();
}
