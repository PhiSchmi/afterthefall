﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LibraryManager : MonoBehaviour
{
    public DataLibrary library;

    public WarbandSO warband;



    private void Awake()
    {
        warband.characters.Clear();

        Debug.Log(warband.name + " initialized.");
        Debug.Log(library.name + " initialized.");

        ShowAllIDsOfType("eq");
        AddToWarband("UnRaiSca1", warband);


    }

    public void ShowAllIDsOfType(string substring)
    {

        
    }

    public void AddToWarband(string id, WarbandSO warband)
    {

        /*  ID_Components______
         *  
         *  Unit            UnFacSqu#Num
         *  Subfaction      sF
         *  Equipment       Eq
         *  
         *  
         *
         */

        if (id.Substring(0, 2).Equals("Un", System.StringComparison.CurrentCultureIgnoreCase))
        {
           
            
        }
        else if (id.Substring(0, 2) == "Eq")
        {
                if (id.Substring(2, 3) == "Wea")
                {
                    for (int j = 0; j < library.equipmentContainer.weapons.Count; j++)
                    {
                        if (id == library.equipmentContainer.weapons[j].ID)
                        {
                            //Take this Weapon
                        }
                        else { Debug.LogWarning("ID" + id + "not found!"); }
                    }
                }
                else if (id.Substring(2, 3) == "Arm")
                {
                    for (int j = 0; j < library.equipmentContainer.armors.Count; j++)
                    {
                        if (id == library.equipmentContainer.armors[j].ID)
                        {
                            //Take this Armor
                        }
                        else { Debug.LogWarning("ID" + id + "not found!"); }
                    }
                }
                else if (id.Substring(2, 3) == "Use")

                    for (int j = 0; j < library.equipmentContainer.usables.Count; j++)
                    {
                        if (id == library.equipmentContainer.usables[j].ID)
                        {
                            //Take this Usable
                        }
                        else { Debug.LogWarning("ID" + id + "not found!"); }
                    }
            
        }
        else if (id.Substring(0, 3) == "Ab_")
        {
            Debug.LogWarning("AbilitySearch not yet implemented");
        }
        else
        {
            Debug.LogWarning("Not allowed naming convention or missing library member: " + id);
        }
    }

}
