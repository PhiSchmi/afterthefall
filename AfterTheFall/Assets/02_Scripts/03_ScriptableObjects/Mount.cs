﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Mount", menuName = "Library/Equipment/Mount")]
public class Mount : Equipment
{
   
    [Header("Modifiers")]
    public int newMovementValue;

    public List<Ability> abilities;
}
