﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment : ScriptableObject
{
    public new string name;
    public string ID;
    public string description;
}
