﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "New Natural Weapon", menuName = "Library/Abilities/Mutation/Natural Weapon")]
public class NaturalWeapon : Mutation
{
    public Weapon weapon;

    public override void Initialize()
    {

    }

    public override void TriggerAbility()
    {

    }
}
