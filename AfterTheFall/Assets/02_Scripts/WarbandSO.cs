﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Warband", menuName = "Warband")]
public class WarbandSO : ScriptableObject
{
    [System.Serializable]
    public class Character
    {
        public string name;
        public string charID; //Char+Name
        public Sprite icon;
        public int movementValue;
        public int sizeValue;
        public int accuracyValue;
        public int command;

        public int resilience;
        public int agility;
        public int strength;
        public int willPower;

        public int armorValue;
        public int healthValue;

        public List<string> abilities;

        public List<string> keywords;

        public List<string> weaponSet1 = new List<string>();
        public List<string> weaponSet2 = new List<string>();

        public string armorID;

        public List<string> equipID;
    }

    public new string name;
    public string ID;       //War+Name

    public int ammo;

    public List<Character> characters = new List<Character>();

    public void CreateIDs()
    {
        ID = "War" + name;
        Debug.Log("ID created for " + name + ": " + ID);

        for (int i = 0; i < characters.Count; i++)
        {
            characters[i].charID = "Char_" + characters[i].name;
            Debug.Log("ID created for " + characters[i].name + ": " + characters[i].charID);
        }
    }
}
