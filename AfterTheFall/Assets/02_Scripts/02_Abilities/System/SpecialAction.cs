﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialAction : ScriptableObject
{
    public new string name = "NEW SPECIAL ACTION";
    public string ID = "XXXXX";
    public string description = "Description Missing!";
    public enum ActionType { Aura, Blast, Blessing, Cone, BulletHell, Outburst, Projectile, Self}
    public ActionType actionType;
    public int usesPerActivation = 1;

    public int shinyCost;

}
