﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "NewBlessing/NewCurse", menuName = "Library/Abilities/BlessingOrCurse")]
public class BlessingOrCurse : Ability
{
    public enum Type { Blessing, Curse }
    public Type type;

    public List<ConditionEnum> conditions;
    public List<BlessingEnum> blessings; 

    public override void Initialize()
    {

    }

    public override void TriggerAbility()
    {

    }
}
