﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ability : ScriptableObject
{
    public new string name = "NEW ABILITY";
    public string ID = "XXXXXX";
    public string description = "DESCRIPTION TEXT MISSING!";
    public FactionsEnum faction;

    public enum AbilityTrigger { OnAwake, OnCriticalHit, OnMiss, Squad, StartActivation, OnEndActivation, OnIncoming, WhileInHealthRange}
    public List<AbilityTrigger> triggers;

    public enum AbilityTarget { Self, AllFriends, AllEnemies, AllOtherSquad, WholeSquad, MeleeAttacker}
    public AbilityTarget target;

    public Enum_AbilityTree abilityTree;

    public bool stackable;

    public abstract void Initialize();
    public abstract void TriggerAbility();
}
