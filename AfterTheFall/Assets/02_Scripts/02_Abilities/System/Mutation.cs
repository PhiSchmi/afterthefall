﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Mutation", menuName = "Library/Abilities/Mutation")]
public class Mutation : Ability
{
    public int sizeModifier;
    public int moveModifier;

    public int armorModifier;
    public int healthModifier;





    public override void Initialize()
    {

    }

    public override void TriggerAbility()
    {

    }
}
