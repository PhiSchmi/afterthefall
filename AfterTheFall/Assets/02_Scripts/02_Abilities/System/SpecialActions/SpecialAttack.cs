﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "New Special Action", menuName = "Library/Special Action/Special Attack")]
public class SpecialAttack : SpecialAction
{
    public AttackTypeEnum attackType;

    public DedicatedSkillEnum dedicatedSkill;

    [Header("Values")]
    public int damage;
    public float critDmgMultiplier = 2.0f;
    public int range;
    public int rate;
    public int aPR;
    public int mod;

    public DamageTypeEnum damageType;

    public List<Ability> abilities;
}
