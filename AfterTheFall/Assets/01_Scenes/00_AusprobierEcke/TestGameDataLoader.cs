﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestGameDataLoader : MonoBehaviour
{
    private Text text;

    public void Start() {
        text = GetComponent<Text>();

        CharacterTemplate ct = GameDataLoader.Instance.GetCharacterTemplateByID("R01_Warchief");
        text.text = $"Name: {ct.Name} Abilities: {ct.Abilities}";
    }
}
