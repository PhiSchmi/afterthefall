﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuControl : MonoBehaviour
{

    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void BTN_Start()
    {
        animator.SetTrigger("TriggerStart");
    }

    public void BTN_Scenario()
    {
        animator.SetTrigger("TriggerScenario");
    }

    public void BTN_NewWarband()
    {
        animator.SetTrigger("TriggerNewWarband");
    }

    public void BTN_SavedWarbands()
    {
        animator.SetTrigger("TriggerSavedWarbands");
    }

    public void BTN_Codex()
    {
        animator.SetTrigger("TriggerCodex");
    }

    public void BTN_Credits()
    {
        animator.SetTrigger("TriggerCredits");
    }


}
